import React, {Component, Fragment} from 'react';
import './Buttons.css';

class App extends Component {

    state = {
        mouseDown: false,
        pixelsArray: [],
        radius: 20,
        color: 'red'
    };

    drawCircles = (x, y, color) => {
        this.context = this.canvas.getContext('2d');
        this.imageData = this.context.createImageData(1, 1);
        this.d = this.imageData.data;
        
        this.context.beginPath();
        this.context.arc(x, y, this.state.radius, 0, 2 * Math.PI, false);
        this.context.fillStyle = color;
        this.context.fill();
    }

    componentDidMount() {
        this.websocket = new WebSocket('ws://localhost:8000/draw');

        this.websocket.onmessage = m => {
            const decodedMessage = JSON.parse(m.data);

            switch (decodedMessage.type) {
                case 'NEW_DRAWING':
                    decodedMessage.pixelsArray.forEach(pixelsPair => {
                        this.drawCircles(pixelsPair.x, pixelsPair.y, decodedMessage.color);
                    });
                    break;
                default: {
                    console.log('something is wrong');
                }
            }
        }
    }


    canvasMouseMoveHandler = event => {

        if (this.state.mouseDown) {
            event.persist();
            this.setState(prevState => {
                return {
                    pixelsArray: [...prevState.pixelsArray, {
                        x: event.clientX,
                        y: event.clientY
                    }]
                };
            });
            this.drawCircles(event.clientX, event.clientY, this.state.color);
        }
    };

    mouseDownHandler = event => {
        this.setState({mouseDown: true});
    };

    mouseUpHandler = event => {

        const message = {
            type: 'ADD_DRAWING',
            pixelsArray: this.state.pixelsArray,
            color: this.state.color
        };
        this.websocket.send(JSON.stringify(message));

        this.setState({mouseDown: false, pixelsArray: []});
    };

    changeCircleColor = (event, color) => {
        event.preventDefault();
        this.setState(prevState => {
            return {...prevState, color}
        });
    }

    render() {
        return (
            <Fragment>

                <canvas
                    ref={elem => this.canvas = elem}
                    style={{border: '1px solid black'}}
                    width={800}
                    height={600}
                    onMouseDown={this.mouseDownHandler}
                    onMouseUp={this.mouseUpHandler}
                    onMouseMove={this.canvasMouseMoveHandler}
                />
                <button onClick={(event) => this.changeCircleColor(event, 'red')}><div className='redButton button'/></button>
                <button onClick={(event) => this.changeCircleColor(event, 'blue')}><div className='blueButton button'/></button>
                <button onClick={(event) => this.changeCircleColor(event, 'black')}><div className='blackButton button'/></button>

            </Fragment>
        );
    }
}

export default App;
