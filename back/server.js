const express = require('express');
const ws = require('express-ws');
const app = express();
const cors = require('cors');
//зачем??
const expressWs = require('express-ws')(app);

const port = 8000;
app.use(cors());

const canvas = {};

app.ws('/draw', (ws, req) => {
    const id = req.get('sec-websocket-key');
    canvas[id] = ws;

    console.log('Client connected.');
    console.log('Number of active connections: ', Object.values(canvas).length);

    ws.on('message', (msg) => {
        let pixels;

        try {
            pixels = JSON.parse(msg);
        } catch (e) {
            return ws.send(JSON.stringify({
                type: 'ERROR',
                message: 'Message is not JSON'
            }));
        }

        switch (pixels.type) {
            case 'ADD_DRAWING' :
                Object.values(canvas).forEach(client => {
                    client.send(JSON.stringify({
                        type: 'NEW_DRAWING',
                        pixelsArray: pixels.pixelsArray,
                        color: pixels.color
                    }))
                });
                break;
            default :
                ws.send(JSON.stringify({
                    type: 'ERROR',
                    message: 'Message is not JSON'
                }));
        }
    })

    ws.on('close', () => {
        delete canvas[id];
        console.log('Client disconnected');
        console.log('Number of active connections: ', Object.values(canvas).length);
    })

});

app.listen(port, () => {
    console.log('server started on port ' + port);
});