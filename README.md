# Group drawing

## How to use

Clone the project

    git clone https://aijanka@bitbucket.org/aijanka/gpoup_drawing.git
    

Start the nodejs server from `back` directory
    
    cd back
    npm run dev

Go back to the root directory

    cd ..

Start the react app from `front` directory

    cd front
    npm start

Enjoy!
